FROM python:3.9

LABEL MAINTAINER Transpetro - INFRA/ARQIS "infrati@transpetro.com.br"

EXPOSE 8080 8443

ENV APIM_USER="webadmin" \
    APIM_USER_HOME="/home/${APIM_USER}"

ENV APIM_HOME=/opt/app/

USER root

RUN apt update
RUN apt install -y build-essential unixodbc-dev wget unzip libffi-dev libaio1 libnsl-dev 

WORKDIR ${APIM_HOME}


RUN python3 -m pip install --upgrade pip
RUN pip install requests fastapi SQLAlchemy

CMD ["python3","main.py"]
